var nodemailer = require('nodemailer')

class emailSendController {

    async doSendEmail(req, res) {
      const param = req.body

      const transporterOptions = {
        service: 'gmail',
        auth: {
          user: process.env.SENDER_EMAIL,
          pass: process.env.SENDER_PASSWORD
        }
      }

      try {
        const transporter = nodemailer.createTransport(transporterOptions)

        const emailOptions = {
          from : process.env.SENDER_EMAIL,
          to: param.to,
          subject: param.subject,
          text: `Hello ${param.to} ! This e-mail ${param.text}`
        }

        const result = await transporter.sendMail(emailOptions)
        console.log(result)
        if(result.accepted.length !== 0) {
          res.status(200).send({
            'status' : 200,
            'accepted': {
              'count': result.accepted.length,
              'accepted_to': result.accepted
            },
            'rejected': {
              'count': result.rejected.length,
              'rejected_to': result.rejected
            },
            'msg': 'Success! ' + result.response
          })
        }
      } catch(err) {
        res.status(500).send({
          'status': 500,
          'msg': err
        })
      }
    }

}

module.exports = emailSendController