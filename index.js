const dotenv = require('dotenv')
dotenv.config()

const express = require('express')
const app = express()
const { fileParser } = require('express-multipart-file-parser')
app.use(fileParser({
  rawBodyOptions: {
    limit: '15mb',
  },
  busboyOptions: {
      limits: {
          fields: 5
        }
      },
  }))

const emailSendController = require('./controllers/emailController.js')
var user = new emailSendController()

app.post('/', user.doSendEmail)

exports.sendEmail = app;